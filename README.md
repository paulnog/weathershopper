# weathershopper

end to end testing of weather shopper

# Introduction 
This task was done using Cypress. with javascript and Nodejs
Machine used was Windows it should also work on other machine
Tested on Chrome browswer it should work on Firefox. not on Safari

This is an e2e ui Test for weathershopper
 https://weathershopper.pythonanywhere.com/

# Installation 
- Clone the repo 

- and change directory to _**weather directory**_ (cd weathershopper)

- and run _**npm install**_ 

this is not install all dependenci
This is easier with VSC IDE
# Run Test
to run test make sure to be in the weathershopper directory
from the command line run

_**npx cypress open**_

# Directories 
**Integration folder** 
1.  _**flink.spec.js**_ the test location

**Pages folder**
1. _**helper.js**_, where list of items are converted to Object and also a function to select least expensive items 
2. _**shopper.js**_ have all the test functions 

# devDependencies 
    cypress  "^8.1.0", Framework
    cypress-iframe "^1.0.1" for handling Iframe

# dependencies
    @testing-library/cypress  "^8.0.0" for finding DOM element
    cypress-xpath "^1.6.2" xpath selector. 

