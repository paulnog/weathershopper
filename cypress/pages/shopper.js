import { obj, toInt, findLeastExpensive} from "./helper"

export function navigate(){
    //Got to ur
    cy.visit("https://weathershopper.pythonanywhere.com/")
}
export function urlValidate(){
    //Assert Url
    cy.url().should("eq","https://weathershopper.pythonanywhere.com/" )
}
export function getTemp(){

    cy.get('#temperature').invoke("text").then((temp) => {
        const temprature = parseInt(temp)
        if (temprature< 19) {
            assert.isBelow(temprature , 19 , "temperature strict lt 19")
            cy.get(':nth-child(1) > a > .btn').click()
        }
        if(temprature > 34) {
            assert.isAbove(temprature , 34 , "temperature strict lt 34")
            cy.get('.offset-4 > a > .btn').click()
        }
    })
}

export function MoisturizersOrSunscreens(){
    // Select items based on temprature 
    cy.get('#temperature').invoke("text").then((temp) => {
        const temprature = parseInt(temp)
        if (temprature< 19) {
            
            cy.get(':nth-child(1) > a > .btn').click()
        
            cy.get('.text-center',{timeout:10000})
            .invoke('text')
            .then((list_items) => {
            // convert arrays of items to aObject
                const diction = obj(list_items)
                // conver to values from Object to int
                let parsedObject = toInt(diction)
                
                //Use thisto search through the object by word e.g. 
                //"Aloe" or "almond" make sure. 
                //to pass in the object with the parsed values
                let aloe = Object.entries(diction).filter(([key, val]) => key
                .includes("Aloe"))
                let almond = Object.entries(diction).filter(([key, val]) => key.
                includes("Almond"))
                
                const wordAloe =  Object.fromEntries(aloe)
                const wordAlmond =Object.fromEntries(almond)
                const containsAloe = (parsedObject,wordAloe )
                const containsAlmond = (parsedObject, wordAlmond)
            
                
                // Find the least expensive 
                const leastExpensiveFromAloeCategory = findLeastExpensive(containsAloe);
                const leastExpensiveFromAlmondCategory = findLeastExpensive(containsAlmond);
                // get items and price 
                const Almond =Object.keys(leastExpensiveFromAlmondCategory).toString()
                const Aloe = Object.keys(leastExpensiveFromAloeCategory).toString()
                const AlmondPrice =Object.values(leastExpensiveFromAlmondCategory).toString()
                const AloePrice = Object.values(leastExpensiveFromAloeCategory).toString()
                const AloeItemPriceRs = "Price: Rs. " + AloePrice
                const AloeItemPrice = "Price: " + AloePrice
                const AlmondItemPriceRs = "Price: Rs. " + AlmondPrice
                const AlmondItemPrice = "Price: " + AlmondPrice
            
                cy.get(".text-center").invoke('text').then((text)=>{
                    if (text.includes(AloeItemPriceRs)){
                        cy.contains(AloeItemPriceRs).next().click() 
                    }else if(text.includes(AloeItemPrice)){
                        cy.contains(AloeItemPrice).next().click()
                    }
                    if (text.includes(AlmondItemPriceRs)){
                        cy.contains(AlmondItemPriceRs).next().click() 
                    }else if(text.includes(AlmondItemPrice)){
                        cy.contains(AlmondItemPrice).next().click()
                    }
                    cy.get('.thin-text').click()
                })
                cy.get('tbody > :nth-child(1) > :nth-child(1)').should('have.text',Aloe)
                cy.get('tbody > :nth-child(1) > :nth-child(2)').should('have.text',AloePrice)
                cy.get('tbody > :nth-child(2) > :nth-child(1)').should('have.text',Almond)
                cy.get('tbody > :nth-child(2) > :nth-child(2)').should('have.text',AlmondPrice)
                //Assert for total cost of items in the cart.
                const totalprice = parseInt( AloePrice) + parseInt(AlmondPrice )
                const totRupees = ("Total: Rupees " + totalprice)
                cy.get('#total').should('have.text', totRupees)
                cy.get('.stripe-button-el > span').click()
            })
    
        }
        if(temprature > 34) {
            //assert.isAbove(temprature , 34 , "temperature strict lt 34")
            cy.get('.offset-4 > a > .btn').click()
            //////////////////////////////////////////////////////////////////////
            
            cy.get('.text-center',{timeout:10000})
            .invoke('text')
            .then((list_items) => {
            // convert arrays of items to aObject
                const diction = obj(list_items)
                // conver to values from Object to int
                let parsedObject = toInt(diction)
                
                //Use this to search through the object by word e.g.
                //"Aloe" or "almond" make sure
                //. to pass in the object with the parsed values
                let SPF50= Object.entries(diction).filter(([key, val]) => key
                .includes("SPF-50"));
                let SPF30 = Object.entries(diction).filter(([key, val]) => key
                .includes("SPF-30"));
        
                const wordSPF50=  Object.fromEntries(SPF50);
                const wordSPF30 =Object.fromEntries(SPF30)
                
                const containsSPF50 = (parsedObject,wordSPF50 );
                const containsSPF30 = (parsedObject, wordSPF30)
                
                //Get least expensive Items
                const leastExpensiveFromSPF50Category = findLeastExpensive(containsSPF50);
                const leastExpensiveFromSPF30Category = findLeastExpensive(containsSPF30);

                ///////////////////////////////////////////////////////////////////////////
                // Add items to the cart
                const sunSPF50 =Object.keys(leastExpensiveFromSPF50Category ).toString()
                const sunSPF30 = Object.keys(leastExpensiveFromSPF30Category ).toString()
                const sunSPF50Price =Object.values(leastExpensiveFromSPF50Category ).toString()
                const sunSPF30Price = Object.values(leastExpensiveFromSPF30Category ).toString()
                const sunSPF50ItemPriceRs = "Price: Rs. " + sunSPF50Price
                const sunSPF50ItemPrice = "Price: " + sunSPF50Price
                const sunSPF30ItemPriceRs = "Price: Rs. " + sunSPF30Price
                const sunSPF30ItemPrice = "Price: " + sunSPF30Price
                //cy.get(".font-weight-bold").contains(Aloe)
                cy.get(".text-center").invoke('text').then((text)=>{
                    if (text.includes(sunSPF50ItemPriceRs)){
                        cy.contains(sunSPF50ItemPriceRs).next().click() 
                    }else if(text.includes(sunSPF50ItemPrice)){
                        cy.contains(sunSPF50ItemPrice).next().click()
                    }
                    if (text.includes(sunSPF30ItemPriceRs)){
                        cy.contains(sunSPF30ItemPriceRs).next().click()
                    }else if(text.includes(sunSPF30ItemPrice)){
                        cy.contains(sunSPF30ItemPrice).next().click()
                    }
                    
                    cy.get('.thin-text').click()
                
                })
            //Assert the list of items in the cart
                cy.get('tbody > :nth-child(1) > :nth-child(1)').should('have.text',sunSPF50)
                cy.get('tbody > :nth-child(1) > :nth-child(2)').should('have.text',sunSPF50Price)
                cy.get('tbody > :nth-child(2) > :nth-child(1)').should('have.text',sunSPF30)
                cy.get('tbody > :nth-child(2) > :nth-child(2)').should('have.text',sunSPF30Price)
                
                
                //Assert total cost of the shooping cart
                const totalprice = parseInt( sunSPF30Price) + parseInt(sunSPF50Price )
                const totRupees = ("Total: Rupees " + totalprice)
                cy.get('#total').should('have.text', totRupees)
                //Click on payment
                cy.get('.stripe-button-el > span').click()

            })
        }
    
    })
}

export function payout(){
    
    cy.frameLoaded('.stripe_checkout_app');
    cy.iframe().find('input[type="email"]').type("flinkapplicant@flink.com")
    cy.iframe().findByPlaceholderText("Card number").type("4242424242424242")
    cy.iframe().findByPlaceholderText("MM / YY").type("08/2021")
    cy.iframe().findByPlaceholderText("CVC").type("08/2021")
    cy.iframe().findByPlaceholderText("ZIP Code").type("Flink")
    cy.iframe().findByText("Pay").click({force: true})
    cy.wait(1000)
    cy.get('h2').should('have.text', 'PAYMENT SUCCESS')

}