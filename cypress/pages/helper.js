export function obj(list_items) {
    const clean_items = /\s*(?:Price: Rs.|Add|Price:|\n|"|$)\s*/
        let get_list = list_items.split(clean_items) 
        let slice_list = get_list.slice(1,13)
        const pairs = slice_list.flatMap((_, i, a) => i % 2 ? [] : [a.slice(i, i + 2)])
        let  listI = new Map(pairs)
        const toObj = Object.fromEntries(listI);

       
    
        
        return toObj
    
}

export function toInt(toObj) {
    let res = Object.entries(toObj).map(([key, val]) => [key, parseInt(val)]);
    const parsedObject = Object.fromEntries(res);
    return parsedObject
}

export function findLeastExpensive(obj) {
            
    let res = Object.entries(obj).sort((arr1, arr2) => arr1[1] - arr2[1]);
    let min = res[0];
    return { [min[0]]: min[1] };
}