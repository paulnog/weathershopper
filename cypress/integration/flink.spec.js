const { navigate, urlValidate, MoisturizersOrSunscreens, payout} = require("../pages/shopper")


describe("Weather Shopper", function(){
    
    it("Go to webiste and select Cream based on Temperature", function() {
      /**Shop for moisturizers if the weather is below 19 degrees.
       Shop for suncreens if the weather is above 34 degrees. */
        navigate()
        urlValidate()
        
        
    })

    it('shop for Moisturizers Or Sunscreens', () => {

      /**
       * Add two moisturizers to your cart. 
       * First, select the least expensive mositurizer that contains Aloe. 
       * For your second moisturizer, s
       * elect the least expensive moisturizer that contains almond. 
       * Click on cart when you are done.
       * 
       * 
       * OR
       * Add two sunscreens to your cart. F
       * irst, select the least expensive sunscreen that is SPF-50. 
       * For your second sunscreen, select the least expensive sunscreen that is SPF-30.
       *  Click on the cart when you are done.
       */
       MoisturizersOrSunscreens()
    });

    it('Payout Success', () => {
      /**
       * Verify if the payment was successful. 
       * The app is setup so there is a 5% chance that your payment failed.
       * 
       */
      payout()
    });

  
      
})
