// in cypress/support/index.ts
// load type definitions that come with Cypress module
/// <reference types="cypress" />

declare namespace Cypress {
    interface Chainable {
      /**Goto web page  
       * @example
       * cy
       *   .gotoPage()
      */
      gotoPage()
      /**Select Items base on the temperature displaced
       * @example
       * cy
       * .tempShop()
       */
      tempShop()
      aloe()
      al1()
    }
  }